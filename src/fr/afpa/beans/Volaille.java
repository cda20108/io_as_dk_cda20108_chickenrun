package fr.afpa.beans;

public abstract class Volaille {

	public static final String BASEID = "200";
	public static final int NB_QuantiteMax = 7;

	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	abstract void modifierPoidAbattage(double poid);

	abstract void modifierPrixAbattage(double prix);

	abstract double getPoid();

	abstract void setPoid(double poid);


}
