package fr.afpa.beans;

import java.util.Scanner;

public class Menu {

	public void affichageMenu(Scanner in) {

		System.out.println("************************************* Menu *************************************");
		System.out.println("*                                                                              *");
		System.out.println("*                     1 - Ajouter une volaille                                 *");
		System.out.println("*                                                                              *");
		System.out.println("*                     2 - Modifier poids abattage                              *");
		System.out.println("*                                                                              *");
		System.out.println("*                     3 - Modifier prix du jour                                *");
		System.out.println("*                                                                              *");
		System.out.println("*                     4 - Modifier poids d'une volaille                        *");
		System.out.println("*                                                                              *");
		System.out.println("*                     5 - Voir le nombre de volaille par type                  *");
		System.out.println("*                                                                              *");
		System.out.println("*                     6 - Voir le total de prix des volailles abattables       *");
		System.out.println("*                                                                              *");
		System.out.println("*                     7 - Vendre une volaille                                  *");
		System.out.println("*                                                                              *");
		System.out.println("*                     8 - Rendre un paon au parc                               *");
		System.out.println("*                                                                              *");
		System.out.println("*                     9 - Quitter l'application                                *");
		System.out.println("*                                                                              *");
		System.out.println("********************************************************************************");

	}

	public void gestionMenu(Scanner in) {

		GestionPoulailler gestionPoulailler = new GestionPoulailler();
		
		affichageMenu(in);
		
		int choix = 0;

		while (choix != 9) {
			System.out.println();
			System.out.println("Veuillez entrer votre choix");
			choix = in.nextInt();
			in.nextLine();

			switch (choix) {

			case 1:
				// ajouter une volaille
				System.out.println("Quelle volaille souhaitez-vous ajouter ?");
				System.out.println(" 1 - Un poulet");
				System.out.println(" 2 - Un canard");
				System.out.println(" 3 - Un paon");
				System.out.println(" 4 - Retour");
				int choixVolaille = in.nextInt();
				in.nextLine();

				switch (choixVolaille) {

				case 1:
					System.out.println(" Veuillez ajouter le poid du volaille");
					double p = in.nextDouble();
					in.nextLine();
					PouletImp poulet = new PouletImp(p);
					gestionPoulailler.ajouterVolaille(poulet);

					break;

				case 2:
					System.out.println("Veuillez entrer le poid du canard");
					double c = in.nextDouble();
					in.nextLine();
					CanardImp canard = new CanardImp(c);
					gestionPoulailler.ajouterVolaille(canard);
					break;

				case 3:
					Paon pa1 = new Paon();
					gestionPoulailler.ajouterVolaille(pa1);
					break;

				case 4:
					affichageMenu(in);
					break;

				default:
					System.out.println("Veuillez choisir le bon element.");

				}

				affichageMenu(in);
				break;

			case 2:

				System.out.println("Quelle poid d'abattage souhaitez-vous modifier ?");
				System.out.println(" 1 - Celui du poulet");
				System.out.println(" 2 - Celui du canard");
				System.out.println(" 3 - Retour");
				int choixAbattage = in.nextInt();
				in.nextLine();

				System.out.println("Veuillez entrer le nouveau poid d'abattage : ");
				int poidAbattage = in.nextInt();
				in.nextLine();

				switch (choixAbattage) {

				case 1:
					PouletImp p = new PouletImp();
					p.modifierPoidAbattage(poidAbattage);

					break;

				case 2:
						CanardImp c = new CanardImp();
						c.modifierPoidAbattage(poidAbattage);
						
					break;

				case 3:

					affichageMenu(in);

					break;

				default:
					System.out.println("Veuillez choisir le bon element.");

				}

				affichageMenu(in);
				break;

			case 3:

				System.out.println("Quelle prix souhaitez-vous modifier ?");
				System.out.println(" 1 - Celui du poulet");
				System.out.println(" 2 - Celui du canard");
				System.out.println(" 3 - Retour");
				choix = in.nextInt();
				in.nextLine();

				System.out.println("Veuillez entrer le nouveau  : ");
				double prixKilo = in .nextDouble();
				in.nextLine();

				switch (choix) {

				case 1:
					
					PouletImp p = new PouletImp();
					p.modifierPrixAbattage(prixKilo);
						
					break;

				case 2:

					CanardImp c = new CanardImp();
					c.modifierPrixAbattage(prixKilo);
					
					break;

				case 3:

					affichageMenu(in);

					break;

				default:
					System.out.println("Veuillez choisir le bon element.");

				}

				affichageMenu(in);
				break;

			case 4:

				System.out.println("Veuillez entrer l'identifiant de la volaille ");
				String id = in.nextLine();
				
				
				if(gestionPoulailler.rechercherVolailleParId(id)!= -1) {
					
					
					if(gestionPoulailler.getListVolailles()[gestionPoulailler.rechercherVolailleParId(id)] instanceof PouletImp  || gestionPoulailler.getListVolailles()[gestionPoulailler.rechercherVolailleParId(id)] instanceof CanardImp) {
						
						System.out.println("Veuillez entrer le nouveau poid de la volaille");
						double poid = in.nextDouble();
						in.nextLine();
						gestionPoulailler.getListVolailles()[gestionPoulailler.rechercherVolailleParId(id)].setPoid(poid);
						
					} else {
						System.out.println("Vous ne pouvez pas modifier le poid d'un paon");
					}
					
					
				}

				affichageMenu(in);
				break;

			case 5:
				// voir le nombre de volaille par type
				gestionPoulailler.voirNbVolaille();
				affichageMenu(in);
				break;

			case 6:
				gestionPoulailler.totalPrixAbattage();
				affichageMenu(in);
				break;

			case 7:
				System.out.println();
				System.out.println("Veuillez entrer l'id de la volaille que vous souhaitez vendre");
				String idChoix = in.nextLine();
				
				gestionPoulailler.supprimerVolaille(idChoix);
				
				affichageMenu(in);
				break;

			case 8:
				System.out.println();
				System.out.println("Veuillez entrer l'id du paon que vous souhaitez rendre au parc");
				String idChoix1 = in.nextLine();
				
				gestionPoulailler.supprimerVolaille(idChoix1);
				
				System.out.print("Votre paon a bien ete rendu au parc");
				affichageMenu(in);
				break;

			default:
				System.out.println("Veuillez choisir le bon element.");
				break;
			}
		}

		System.exit(0);

	}

	
}
