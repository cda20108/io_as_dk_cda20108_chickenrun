package fr.afpa.beans;

public class PouletImp extends Volaille implements IConsommable {

	private static double poidAbattagePoulet;
	private static double prixAbattagePoulet;
	private static int nbPoulet;
	private double poid;

	public PouletImp(double poid) {

		this.poid = poid;
		super.setId(BASEID + "PO" + (++nbPoulet));

	}

	public PouletImp() {
		
	}



	public static double getPoidAbattagePoulet() {
		return poidAbattagePoulet;
	}

	public static int getNbPoulet() {
		return nbPoulet;
	}

	public static void setNbPoulet(int nbPoulet) {
		PouletImp.nbPoulet = nbPoulet;
	}

	public static void setPrixAbattagePoulet(double prix) {
		prixAbattagePoulet = prix;
	}

	public static double getPrixAbattagePoulet() {

		return prixAbattagePoulet;

	}
	
	@Override
	public boolean comparaisonPoid() {

		if (poid >= poidAbattagePoulet) {

			return true;

		}

		return false;
	}

	@Override
	void modifierPoidAbattage(double poid) {
		 poidAbattagePoulet = poid; 
		
	}

	@Override
	void modifierPrixAbattage(double prix) {
		prixAbattagePoulet = prix;
		
	}

	

	@Override
	void setPoid(double poid) {
		this.poid = poid;
		
	}

	@Override
	double getPoid() {
	
		return this.poid;
	}


	
	
	



}
