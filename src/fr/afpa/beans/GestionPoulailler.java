package fr.afpa.beans;

import java.text.DecimalFormat;
import java.util.Scanner;

public final class GestionPoulailler {

	private Volaille[] listVolailles;
	private CanardImp canard;
	private PouletImp poulet;

	Scanner in = new Scanner(System.in);

	DecimalFormat df = new DecimalFormat(".00");

	public GestionPoulailler() {

		listVolailles = new Volaille[Volaille.NB_QuantiteMax];

	}
	
	

	public Volaille[] getListVolailles() {
		return listVolailles;
	}


	public final void voirNbVolaille() {

		System.out.println("Il y a " + CanardImp.getNbCanard() + " canard(s), " + PouletImp.getNbPoulet()
		+ " poulet(s) et " + Paon.nbPaon + " paon(s) dans le poulailler.");
		

		if (CanardImp.getNbCanard() > 0) {
			afficherCanard();
		}
		
		if (PouletImp.getNbPoulet() > 0) {
			afficherPoulet();
		}
		
		if (Paon.getNbPaon() > 0) {
			afficherPaon();
		}
		

	}

	public final void totalPrixAbattage() {

		double totalprixAbattage = 0;

		for (int i = 0; i < listVolailles.length; i++) {

			if(listVolailles[i] != null && listVolailles[i] instanceof CanardImp) {
				
				canard = (CanardImp) listVolailles[i];
				
				if(canard.comparaisonPoid()) {
					totalprixAbattage += CanardImp.getPrixAbattageCanard() * listVolailles[i].getPoid();
				}
			}
			
			if(listVolailles[i] != null && listVolailles[i] instanceof PouletImp) {
				
				poulet = (PouletImp) listVolailles[i];
				
				if(poulet.comparaisonPoid()) {
					totalprixAbattage += PouletImp.getPrixAbattagePoulet() * listVolailles[i].getPoid();
				}
		
			}
		}

		System.out.println("Le prix total de l'abattage du jour est de : " + df.format(totalprixAbattage) + "euros.");
	}

	public void ajouterVolaille(Volaille volaille) {

		for (int i = 0; i < listVolailles.length; i++) {

			// ======> Canard

			if (listVolailles[i] == null && volaille instanceof CanardImp) {

				if (CanardImp.getNbCanard() <= 4) {
					listVolailles[i] = volaille;

					System.out.println("Vous avez ajoute un canard qui possede desormais l'id " + volaille.getId());
					break;
				}

				else {
					System.out.println("Vous avez deja atteint le nombre maximum de canard dans ce poulailler.");
					break;
				}
			}

			// ======> Poulet

			else if (listVolailles[i] == null && volaille instanceof PouletImp) {

				if (PouletImp.getNbPoulet() <= 5) {
					listVolailles[i] = volaille;

					System.out.println("Vous avez ajoute un poulet qui possede desormais l'id " + volaille.getId());
					break;
				}

				else {
					System.out.println("Vous avez deja atteint le nombre maximum de poulet dans ce poulailler.");
					break;
				}
			}

			// ======> Paon

			else if (listVolailles[i] == null && volaille instanceof Paon) {

				if (Paon.getNbPaon() <= 3) {

					listVolailles[i] = volaille;

					System.out.println("Vous avez ajoute un paon qui possede desormais l'id " + volaille.getId());
					break;
				}

				else {
					System.out.println("Vous avez deja atteint le nombre maximum de paon dans ce poulailler.");
					break;
				}
			}
		}

		if (PouletImp.getNbPoulet() == 5 && CanardImp.getNbCanard() == 4 && Paon.nbPaon == 3) {
			System.out.println("Le nombre de volaille maximum a ete atteins.");
		}
	}

	/**
	 * Methode de suppression d'une volaille
	 *
	 * @param idVolaille : identifiant d'une volaille
	 */
	public void supprimerVolaille(String idVolaille) {

		if (rechercherVolailleParId(idVolaille) == -1) {

			System.out.println("Erreur volaille introuvable");

		} else {

			if (listVolailles[rechercherVolailleParId(idVolaille)] instanceof PouletImp) {

				PouletImp.setNbPoulet(PouletImp.getNbPoulet() - 1);

			} else if (listVolailles[rechercherVolailleParId(idVolaille)] instanceof CanardImp) {

				CanardImp.setNbCanard(CanardImp.getNbCanard() - 1);

			} else if (listVolailles[rechercherVolailleParId(idVolaille)] instanceof Paon) {

				Paon.setNbPaon(Paon.getNbPaon() - 1);

			}

			listVolailles[rechercherVolailleParId(idVolaille)] = null;

		}

	}

	public int rechercherVolailleParId(String idVolaille) {

		for (int i = 0; i < listVolailles.length; i++) {

			if (listVolailles[i] != null && listVolailles[i].getId().equalsIgnoreCase(idVolaille)) {

				return i;
			}

		}

		return -1;

	}

	
	public void afficherPoulet() {
		
		System.out.println("Liste des poulets : ");
		
		 for (int i = 0; i < listVolailles.length; i++) {
			
			 if(listVolailles[i] != null && listVolailles[i] instanceof PouletImp) {
				 
				 System.out.println("Le poulet : " + listVolailles[i].getId() + " poids : " + listVolailles[i].getPoid());
				 
			 }
		}
	 
	}
	
	public void afficherCanard() {
		
		System.out.println("Liste des Canards : ");
		
		 for (int i = 0; i < listVolailles.length; i++) {
			
			 if(listVolailles[i] != null && listVolailles[i] instanceof CanardImp) {
				 
				 System.out.println("Le canard : " + listVolailles[i].getId() + " poids : " + listVolailles[i].getPoid());
				 
			 }
		}
	 
	}
	
	public void afficherPaon() {
		
		System.out.println("Liste des Paons  : ");
		
		 for (int i = 0; i < listVolailles.length; i++) {
			
			 if(listVolailles[i] != null && listVolailles[i] instanceof Paon) {
				 
				 System.out.println("Le paon : " + listVolailles[i].getId());
				 
			 }
		}
	 
	}


}
