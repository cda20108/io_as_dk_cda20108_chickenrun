package fr.afpa.beans;

public class CanardImp extends Volaille implements IConsommable {
	
	
	private static double poidAbattageCanard;
	private static double prixAbattageCanard;
	private static int nbCanard;
	private double poid ; 
	
	public CanardImp(double poid) {
		super();
		this.poid = poid;
		super.setId(BASEID + "CA" + (++nbCanard));
	}
	
	public CanardImp() {
		
	}
	
	@Override
	public void modifierPoidAbattage(double poid) {
		poidAbattageCanard = poid;
	}

	@Override
	public void modifierPrixAbattage(double prix) {
		prixAbattageCanard = prix;
	}
	
	@Override
	double getPoid() {
		return this.poid;
	}
	
	@Override
	void setPoid(double poid) {
		this.poid = poid;
	}

	@Override
	public boolean comparaisonPoid() {

		if (poid >= poidAbattageCanard) {

			return true;

		}

		return false;
	}
	
	



	public static int getNbCanard() {
		return nbCanard;
	}

	public static void setNbCanard(int nbCanard) {
		CanardImp.nbCanard = nbCanard;
	}

	public static double getPoidAbattageCanard() {
		return poidAbattageCanard;
	}

	public static double getPrixAbattageCanard() {
		return prixAbattageCanard;
	}
	
	







}
